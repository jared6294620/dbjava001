/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

/**
 *
 * @author jared
 */

import modelo.*;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JOptionPane;
import vista.jifproductos;

public class Controlador implements ActionListener{
    private dbProducto db;
    private jifproductos vista;
    private boolean esActualizar;
    private int idProducto;
    
    // Constructor

    public Controlador(dbProducto db, jifproductos vista) {
        this.db = db;
        this.vista = vista;
        
        // Escuchar el evento clic de los siguientes botones
        vista.btnBuscar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
    }
    
    // Métodos helpers
    
    public void limpiar() {
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.jdcFecha.setDate(new Date());
    }
    
    public void cerrar() {
        int res = JOptionPane.showConfirmDialog(vista, "Desea cerrar el sistema ",
                "Productos", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        
        if (res == JOptionPane.YES_OPTION) {
            vista.dispose();
        }
    }
    
    public void habilitar() {
        vista.txtCodigo.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnDeshabilitar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
    }
    
    public void deshabilitar() {
        vista.txtCodigo.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtPrecio.setEnabled(false);
        vista.btnBuscar.setEnabled(false);
        vista.btnDeshabilitar.setEnabled(false);
        vista.btnNuevo.setEnabled(false);
    }
    
    public boolean validar() {
        boolean exito = true;
        
        if(vista.txtCodigo.getText().equals("") || vista.txtNombre.getText().equals("")
                || vista.txtPrecio.getText().equals("")) exito = false;
        
        return exito;
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == vista.btnLimpiar) this.limpiar();
        if(ae.getSource() == vista.btnCancelar) {
            this.limpiar(); this.deshabilitar();
        }
        if(ae.getSource() == vista.btnCerrar) this.cerrar();
        if(ae.getSource() == vista.btnNuevo) {
            this.limpiar(); this.habilitar(); this.esActualizar = false; vista.txtCodigo.requestFocus();
        }
        
        if(ae.getSource() == vista.btnBuscar) {
            Productos pro = new Productos();
            // Validar
            if(vista.txtCodigo.equals("")) {
                JOptionPane.showMessageDialog(vista, "Favor de capturar el código");
                vista.txtCodigo.requestFocus();
            } else {
                try {
                    pro = (Productos) db.buscar(vista.txtCodigo.getText());
                    if(pro.getIdProductos() != 0) {
                        // Mostrar información
                        vista.txtNombre.setText(pro.getNombre());
                        vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                        // Pendiente la fecha
                        vista.btnGuardar.setEnabled(true);
                        vista.btnDeshabilitar.setEnabled(true);
                        this.esActualizar = true;
                        this.idProducto = pro.getIdProductos();
                    } else {
                        JOptionPane.showMessageDialog(vista, "No se encontró producto con código " + vista.txtCodigo.getText());
                    }
                } catch(Exception e) {
                    JOptionPane.showMessageDialog(vista, "Surgió error al buscar " + e.getMessage());
                }
            }
        }
        if(ae.getSource() == vista.btnGuardar) {
            // Hacer el objeto de la clase producto
            Productos pro = new Productos();
            
            if(this.validar() == true) {
                // Todo bien
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setNombre(vista.txtNombre.getText());
                pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                pro.setFecha("2024-07-05");
                try {
                    if(this.esActualizar == false) {
                        // Insertar tabla
                        db.insertar(pro);
                        JOptionPane.showConfirmDialog(vista, "Se agregó con éxito el producto con código " + vista.txtCodigo);
                        this.limpiar();
                        this.deshabilitar();
                    } else {
                        // Actualizar tabla
                        db.actualizar(pro);
                        JOptionPane.showConfirmDialog(vista, "Se actualizó con éxito el producto con código " + vista.txtCodigo);
                        this.limpiar();
                        this.deshabilitar();
                    }    
                } catch(Exception e) {
                    JOptionPane.showMessageDialog(vista, "Surgió un error al insertar producto " + e.getMessage());
                }
            } else JOptionPane.showMessageDialog(vista, "Faltó información");
        }
        
        if(ae.getSource() == vista.btnDeshabilitar) {
            Productos pro = new Productos();
            pro.setIdProductos(this.idProducto);
            
            int res = JOptionPane.showConfirmDialog(vista, "¿Desea deshabilitar el producto?", "Productos", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if(res == JOptionPane.YES_OPTION) {
                try {
                    db.deshabilitar(pro);
                    this.limpiar();
                    this.deshabilitar();
                    // Actualizar la tabla
                } catch(Exception e) {
                    JOptionPane.showConfirmDialog(vista, "Surgió un error al deshabilitar el producto " + e.getMessage());
                }
            }
        }
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
